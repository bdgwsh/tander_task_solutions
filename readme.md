# Решения тестового задания
В директирии solutions расположены файлы с решениями задач.

## Требования
* python3.6
* модули из файла requirements.txt

## Установка модулей и запуск
1. Создать виртуальное окружение с python >= 3.6 `$ virtualenv --python=python3.6 venv`
2. Активировать виртуальное окружение `$ source venv/bin/activate`
3. Установить зависимости из файла requirements.txt `pip install -r requirements`
4. Запустить решение `$ python solutions/task__x__.py`

## Запуск тестов
Запуск тестов осуществялется командой `$ python -m unittest solutions.tests.test_file`.
Например, `$ python -m unittest solutions.tests.test_task1`.

При запуске тестов необходимо находиться в корневой директории проекта.


## Задание 5 в Jupyter Notebook
Для корректного использования `gmaps` в jupyter необходимо создать свой собственный <a href="https://jupyter-gmaps.readthedocs.io/en/latest/authentication.html">токен</a> для использования `GoogleMaps API` и включить расширение:

`$ jupyter nbextension enable --py --sys-prefix gmaps`

<img src="img/roads.png"
     alt="roads"
     style="float: left; margin-right: 10px;" />

<img src="img/path.png"
     alt="path"
     style="float: left; margin-right: 10px;" />
