from functools import wraps
import time

"""
Описать декоратор для функции, выводящий название функции,
переданные ей аргументы и время её выполнения.
"""


def debug(func, prefix="function called: "):

    @wraps(func)
    def wrapper(*args, **kwargs):

        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        elapsed = end_time - start_time
        passed_args = [str(arg) for arg in args]

        if kwargs:
            args_pairs = [f"{arg}={value}" for arg, value in kwargs.items()]
            passed_args.append(', '.join(arg_pair for arg_pair in args_pairs))

        print(f"[{elapsed:.6f}s] {prefix} {func.__name__}({', '.join(arg for arg in passed_args)}) ")
        return result

    return wrapper


@debug
def test(*args, **kwargs):
    return sum(args)


if __name__ == '__main__':
    print(test(1, 2, c=4, d=2))
    print(test(*range(10), c=2, d=2231, abs=3))