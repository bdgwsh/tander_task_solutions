"""
Даны три пары точек:

1. A1(xa1, ya1), A2(xb2, yb2)

2. B1(xb1, yb1), B2(xb2, yb2)

3. С1(xс1, yс1), С2(xс2, yс2)

Через данные пары точек проходят прямые a, b, c соответственно.

Определить отношение между прямыми (три прямые параллельны; две прямые параллельны; три прямые пересекаются и т.д.).

Если три прямые пересекаются, более чем в одной точке, то они образуют треугольник.

Вычислить площадь этого треугольника.
"""
from itertools import combinations
import string
import numpy as np
from .utils import Point2D


class Line2D:

    def __init__(self, A, B):
        assert isinstance(A, Point2D)
        assert isinstance(B, Point2D)
        self.A = A
        self.B = B
        self.y1 = self.A.y
        self.y2 = self.B.y
        self.x1 = self.A.x
        self.x2 = self.B.x

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return f'({self.x1};{self.y1}):({self.x2};{self.y2})'

    @property
    def slope(self):
        try:
            slope = (self.y2 - self.y1) / (self.x2 - self.x1)
        except ZeroDivisionError:
            slope = float('inf')
        return slope

    @classmethod
    def are_parralel(cls, a, b):
        assert isinstance(a, cls)
        assert isinstance(b, cls)
        return a.slope == b.slope

    @classmethod
    def are_intersecting(cls, line1, line2):
        assert isinstance(line1, cls)
        assert isinstance(line1, cls)
        lines_are_intersecting = not cls.are_parralel(line1, line2)
        cross_point = None
        if lines_are_intersecting:
            a1 = line1.y1 - line1.y2
            b1 = line1.x2 - line1.x1
            a2 = line2.y1 - line2.y2
            b2 = line2.x2 - line2.x1
            d = a1 * b2 - a2 * b1
            c1 = line1.y2 * line1.x1 - line1.x2 * line1.y1
            c2 = line2.y2 * line2.x1 - line2.x2 * line2.y1

            x = (b1 * c2 - b2 * c1) / d
            y = (a2 * c1 - a1 * c2) / d
            cross_point = Point2D(x, y)
        return lines_are_intersecting, cross_point

    @classmethod
    def inspect_lines(cls, lines):
        parrallel_lines = set()
        intersecting_lines = set()
        cross_points = []
        for pair in combinations(lines, r=2):
            first_line, second_line = lines[pair[0]], lines[pair[1]]
            first_line_name, second_line_name = pair[0], pair[1]
            if Line2D.are_parralel(first_line, second_line):
                parrallel_lines.add(first_line_name)
                parrallel_lines.add(second_line_name)

            are_intersecting, cross_point = Line2D.are_intersecting(first_line, second_line)
            if are_intersecting:
                cross_points.append(cross_point)
                intersecting_lines.add(first_line_name)
                intersecting_lines.add(second_line_name)

        parrallel_lines = sorted(list(parrallel_lines))
        intersecting_lines = sorted(list(intersecting_lines))

        return parrallel_lines, intersecting_lines, cross_points

    @classmethod
    def get_lines_from_points(cls, points):
        lines = {}
        char_index = 0
        for i, _ in enumerate(points):
            if (i + 1) % 2 == 0:
                a = points[i - 1]
                b = points[i]
                line_name = string.ascii_letters[char_index]
                lines[line_name] = Line2D(a, b)
                char_index += 1

        return lines


class Triangle:

    def __init__(self, a=None, b=None, c=None):
        self.A = a
        self.b = b
        self.c = c
        self.x1 = a.x
        self.x2 = b.x
        self.x3 = c.x
        self.y1 = a.y
        self.y2 = b.y
        self.y3 = c.y

    def get_total_area(self):
        area = abs(np.linalg.det(np.array([
            [self.x1 - self.x3, self.y1 - self.y3],
            [self.x2 - self.x3, self.y2 - self.y3]
        ])) / 2)
        return area


def get_points_from_input(num_lines):
    points = []
    for i in range(num_lines):
        coordinates = list(map(float, input().strip().split()))
        A = Point2D(coordinates[0], coordinates[1])
        points.append(A)
        B = Point2D(coordinates[2], coordinates[3])
        points.append(B)

    return points


def get_area_answer(intersecting_lines, cross_points):
    area = 0
    if len(cross_points) > 2:
        A, B, C = cross_points[0], cross_points[1], cross_points[2]
        triangle = Triangle(A, B, C)
        area = triangle.get_total_area()

    return area


def get_lines_inspection_result(points):
    lines = Line2D.get_lines_from_points(points)
    parrallel_lines, intersecting_lines, cross_points = Line2D.inspect_lines(lines)
    inspection_string = ' || '.join(parrallel_lines)
    if len(parrallel_lines) != 2:
        inspection_string = ' ^ '.join(intersecting_lines)

    area = get_area_answer(intersecting_lines, cross_points)
    return inspection_string, area


if __name__ == '__main__':
    points = get_points_from_input(3)
    for result in get_lines_inspection_result(points):
        print(result)

