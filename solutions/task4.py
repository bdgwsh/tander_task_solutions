"""
Пусть две точки A и B являются симметричными относительно прямой d,
если отрезок АВ перпендикулярен прямой d, и расстояние от точек A и B до прямой d одинаково.

Дано множество точек на плоскости, количество точек чётное:
(x1, y1), …, (xn, yn)
Определить, существует ли прямая d, параллельная одной из осей координат,
которая разбивает данное множество на два подмножества,
таких что для каждой точки A(xi, yi) из первого подмножества существует точка B(xj yj),
которая симметрична точке A относительно прямой d.

Если прямая существует, вывести "YES", иначе "NO".
"""
from itertools import combinations
from collections import Counter
from .utils import Point2D


def check_y_symmetry(points):
    """
    Returns True if points have symmetry by line x = const

    y           x = 2.5
    ^         /
    |A*       |       *B
    |         |
    |     C*  |  *D
    |       E*|*F
    ------------------> x
            2.5
    :param points:
    :return: points_are_symmetrical
    """
    # find all pairs that are parallel to x  [(A, B), (C, D), (E, F)]
    pairs = [pair for pair in combinations(points, r=2) if pair[0].y == pair[1].y]
    mid_xs = []
    for pair in pairs:
        mid_x = min(pair[0].x, pair[1].x) + abs(pair[0].x - pair[1].x) / 2
        mid_xs.append(mid_x)
    mid_xs = Counter(mid_xs)
    common_mid_x = mid_xs.most_common(n=1)[0][1] if len(mid_xs) > 0 else None
    if common_mid_x != len(points)/2:
        return False
    return True


def check_x_symmetry(points):
    """
    Returns True if points have symmetry by line y = const

    y
    ^
    |  A     B
    |  *     *            y = 2.5
2.5 |___________________/
    |
    |  *C    *D
    ------------------> x
            2.5
    :param points:
    :return: points_are_symmetrical
    """
    pairs = [pair for pair in combinations(points, r=2) if pair[0].x == pair[1].x]
    mid_ys = []
    for pair in pairs:
        mid_y = min(pair[0].y, pair[1].y) + abs(pair[0].y - pair[1].y) / 2
        mid_ys.append(mid_y)

    mid_ys = Counter(mid_ys)
    common_mid_y = mid_ys.most_common(n=1)[0][1] if len(mid_ys) > 0 else None
    if common_mid_y != len(points)/2:
        return False
    return True


def check_points_symmetry(points):
    is_symmetrical_by_y = check_y_symmetry(points=points)
    is_symmetrical_by_x = check_x_symmetry(points=points)
    return any([is_symmetrical_by_y, is_symmetrical_by_x])


def get_points_from_input(num_points):
    points = []
    for i in range(num_points):
        coordinates = list(map(float, input().strip().split()))
        point = Point2D(coordinates[0], coordinates[1])
        points.append(point)

    return points


def get_points_inspection_result(points):
    points_are_symmetric = check_points_symmetry(points=points)
    return "YES!" if points_are_symmetric else "NO!"


def main():
    points = get_points_from_input(4)
    print(get_points_inspection_result(points=points))


if __name__ == '__main__':
    main()
