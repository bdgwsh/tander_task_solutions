class Point2D:
    """
    Class that represents a point in 2D space.
    """

    def __init__(self, x=0.0, y=0.0):
        self.x = float(x)
        self.y = float(y)

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return f'({self.x};{self.y})'
