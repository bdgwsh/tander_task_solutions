import unittest
from .testing_utils import TaskAnswer
from solutions.utils import Point2D
from solutions.task4 import get_points_inspection_result


class TestTask4(unittest.TestCase):

    def setUp(self):
        self.test_set = [
            TaskAnswer(
                input_data=[
                    Point2D(1.0, 1.0), Point2D(0.5, 1.5),
                    Point2D(1.0, 2.0), Point2D(2.0, 2.0),
                    Point2D(2.0, 1.0), Point2D(2.5, 1.5)
                ],
                answer='YES!'
            ),
            TaskAnswer(
                input_data=[
                    Point2D(-1.0, 2.0), Point2D(1.0, 2.0),
                    Point2D(-2.0, 2.0), Point2D(2.0, 2.0),
                ],
                answer='YES!'
            ),
            TaskAnswer(
                input_data=[
                    Point2D(-1.0, 2.0), Point2D(4.0, 1.0),
                    Point2D(-2.0, 2.0), Point2D(2.0, 2.0),
                ],
                answer='NO!'
            ),
            TaskAnswer(
                input_data=[
                    Point2D(2.0, -1.0), Point2D(2.0, 1.0),
                    Point2D(2.0, -2.0), Point2D(2.0, 2.0),
                ],
                answer='YES!'
            ),
            TaskAnswer(
                input_data=[
                    Point2D(2.0, -1.0), Point2D(2.0, 1.0),
                    Point2D(2.0, -5.0), Point2D(2.0, 2.0),
                ],
                answer='NO!'
            ),
            TaskAnswer(
                input_data=[
                    Point2D(2.0, -1.0), Point2D(2.0, 1.0),
                    Point2D(3.0, -2.0), Point2D(2.0, 2.0),
                ],
                answer='NO!'
            )
        ]

    def test_solution(self):
        for test_item in self.test_set:
            points = test_item.input
            predicted_answer = get_points_inspection_result(points)
            self.assertEqual(predicted_answer, test_item.answer,
                             f'Input Points: {points}')
