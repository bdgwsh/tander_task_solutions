import unittest
from .testing_utils import TaskAnswer
from solutions.utils import Point2D
from solutions.task1 import get_lines_inspection_result


class TestTask1(unittest.TestCase):

    def setUp(self):
        self.test_set = [
            TaskAnswer(
                input_data=[
                    Point2D(0.0, 0.0), Point2D(2.0, 0.0),
                    Point2D(2.0, 0.0), Point2D(2.0, 2.0),
                    Point2D(2.0, 2.0), Point2D(0.0, 0.0)
                ],
                answer=('a ^ b ^ c', 2.0)
            ),
            TaskAnswer(
                input_data=[
                    Point2D(1.0, 1.0), Point2D(2.0, 1.5),
                    Point2D(1.0, 1.5), Point2D(2.0, 2.0),
                    Point2D(2.0, 3.0), Point2D(3.0, 1.5)
                ],
                answer=('a || b', 0.0)
            )
        ]

    def test_solution(self):
        for test_item in self.test_set:
            points = test_item.input
            predicted_answer = get_lines_inspection_result(points)
            self.assertEqual(predicted_answer, test_item.answer,
                             f'Input points: {points}')
