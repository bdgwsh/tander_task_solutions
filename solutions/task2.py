"""
Описать класс, который хранит список всех своих экземпляров, доступ к которому можно получить с помощью метода getInstances().

class MyClass():

    __instances = []

    …

A = MyClass()
B = MyClass()

for obj in MyClass.getInstances()):
"""


class Foo:
    __instances = []

    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls, *args, **kwargs)
        cls.__instances.append(obj)
        return obj

    @classmethod
    def getInstances(cls):
        return cls.__instances


if __name__ == '__main__':
    a = Foo()
    b = Foo()
    c = Foo()
    print(Foo.getInstances())
